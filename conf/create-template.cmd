curl -XPUT http://localhost:9200/_template/virgin_tt_template -d '
{
"template": "virgin-tt-*",
"order":    1,
"settings": {
"number_of_shards": 5
},
"mappings": {
"virgin-tt": {
"_all" : {"enabled" : false},
"properties": {
"Status": {
"type": "string",
"index":  "not_analyzed"
},
"Impact":  {
"type":   "string",
"index":  "not_analyzed"
},
"Urgency":  {
"type":   "string",
"index":  "not_analyzed"
},
"Priority":  {
"type":   "string",
"index":  "not_analyzed"
},
"Stage Condition":  {
"type":   "string",
"index":  "not_analyzed"
},
"Current Stage":  {
"type":   "string",
"index":  "not_analyzed"
},
"Site Group":  {
"type":   "string",
"index":  "not_analyzed"
},
"Department":  {
"type":   "string",
"index":  "not_analyzed"
},
"Country":  {
"type":   "string",
"index":  "not_analyzed"
},
"Region":  {
"type":   "string",
"index":  "not_analyzed"
},
"Submitter":  {
"type":   "string",
"index":  "not_analyzed"
},
"Owner Group":  {
"type":   "string",
"index":  "not_analyzed"
},
"Owner":  {
"type":   "string",
"index":  "not_analyzed"
},
"Assigned Group":  {
"type":   "string",
"index":  "not_analyzed"
},
"Resolved User":  {
"type":   "string",
"index":  "not_analyzed"
},
"Closed User":  {
"type":   "string",
"index":  "not_analyzed"
},
"Last Modified By":  {
"type":   "string",
"index":  "not_analyzed"
},
"Product Name":  {
"type":   "string",
"index":  "not_analyzed"
},
"Manufacturer":  {
"type":   "string",
"index":  "not_analyzed"
},
"Reported Date":  {
"type":   "date",
"format": "epoch_second"
},
"Last Acknowledged Date":  {
"type":   "date",
"format": "epoch_second"
},
"Required Resolution Date Time":  {
"type":   "date",
"format": "epoch_second"
},
"Estimated Resolution Date":  {
"type":   "date",
"format": "epoch_second"
},
"Last Resolved Date":  {
"type":   "date",
"format": "epoch_second"
},
"Last Modified Date Time":  {
"type":   "date",
"format": "epoch_second"
},
"Re opened Date":  {
"type":   "date",
"format": "epoch_second"
},
"Closed Date":  {
"type":   "date",
"format": "epoch_second"
},
"New Time":  {
"type":   "date",
"format": "epoch_second"
},
"Assigned Time":  {
"type":   "date",
"format": "epoch_second"
},
"In Progress Time":  {
"type":   "date",
"format": "epoch_second"
},
"Cancelled Time":  {
"type":   "date",
"format": "epoch_second"
},
 "Pending Time":  {
 "type":   "date",
 "format": "epoch_second"
 }



}
}
}
}
';