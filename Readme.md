# Introduction 

The purpose of this page is to provide an overview of the work done as part of implementing a simple demo system providing dashboards on top of Virgin Medias trouble ticket data.

You can read more about it here: [Virgin Demo Page On Confluence](https://dev.sonalake.com/confluence/display/DD/Virgin+Demo)

In particular the following page provides a description of the proposed work [Outline Proposal Page on Confluence](https://dev.sonalake.com/confluence/display/DD/Outline+Proposal) with the provision that, in the end it was not
necessary to write a pre-processor because the trouble ticket data from Virgin only included tickets for Germany so it did not make sense to enrich the records with country based Lat Long
informaiton as there was only one country involved.

# Summary

In summary the following work was carried out:

* The ELK stack was installed on the existing VisiMetrix AWS demo server (see installation steps below).
* A logstash config file was created for loading the CSV tickets (see below for details).
* An Elastic Search Index template was created for mapping the loaded records to specific types (see below for details).
* The data was loaded into ElasticSearch using logstash.
* A collection of dashboards and visualisations were created using Kibana.

# Logstash config File

The following logstash config file was used to load the data:

```
input {
    # read csv data in
    file {
        path => "/home/virgin/data/demo/input/*.csv"
        start_position => "beginning"
        type => "virgin-tt"
        sincedb_path => "/dev/null"
    }
}
filter {
    # use the cdr filter and give all columns proper names
    csv {
        separator => ","
        columns => [
        "Incident Number", "Description", "Status", "Status Reason",
                "Impact", "Urgency", "Priority",  "Stage Condition", "Current Stage",
                "Contact Company", "Organisation", "Site Group", "Site", "Department", "Country",
                "Region", "Submitter", "Owner Support Copmany", "Owner Support Organisation", "Owner Group", "Owner",
                "Assigned Support Company" ,"Assigned Support Organisation" , "Assigned Group" , "Resolved User",
                "Closed User", "Last Modified By" ,"Service CI" , "Categoristion Tier 1" , "Categoristion Tier 2" ,
                "Categoristion Tier 3" , "Product Categorisation Tier 1", "Product Categorisation Tier 2",
                "Product Categorisation Tier 3", "Product Name", "Product Model Version", "Manufacturer",
                "Resolution Category", "Resolution Category Tier 2", "Resolution Category Tier 3", "Closure Product Category Tier 1",
                "Closure Product Category Tier 2", "Closure Product Category Tier 3", "Closure Product Name",
                "Closure Product Model Version", "Closure Manufacturer", "Resolution","Resolution Method",
                "Generic Categorisation Tier 1", "Closure Source 1", "Impacted Customers",
                "Reported Date",
                "Last Acknowledged Date",
                "Required Resolution Date Time",
                "Estimated Resolution Date",
                "Last Resolved Date",
                "Last Modified Date Time",
                "Re opened Date",
                "Closed Date", "New Time",
                "Assigned Time",
                "In Progress Time",
                "Cancelled Time",
                "Pending Time"
        ]
    }

    mutate
    {
         remove_field => [ "message", "path", "host" ]
    }

}

# and finally output to elasticsearch
output {
    elasticsearch {
        hosts => ["localhost:9200"]
        index => "virgin-tt-%{+YYYY.MM.dd}"
        manage_template => false

    }
}

```

# Index Template
The following template was used for the index in ElasticSearch.
 
```
{
  "template": "virgin-tt-*",
  "order": 1,
  "settings": {
    "number_of_shards": 5
  },
  "mappings": {
    "virgin-tt": {
      "_all": {
        "enabled": false
      },
      "properties": {
        "Status": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Impact": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Urgency": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Priority": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Stage Condition": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Current Stage": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Site Group": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Department": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Country": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Region": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Submitter": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Owner Group": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Owner": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Assigned Group": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Resolved User": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Closed User": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Last Modified By": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Product Name": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Manufacturer": {
          "type": "string",
          "index": "not_analyzed"
        },
        "Reported Date": {
          "type": "date",
          "format": "epoch_second"
        },
        "Last Acknowledged Date": {
          "type": "date",
          "format": "epoch_second"
        },
        "Required Resolution Date Time": {
          "type": "date",
          "format": "epoch_second"
        },
        "Estimated Resolution Date": {
          "type": "date",
          "format": "epoch_second"
        },
        "Last Resolved Date": {
          "type": "date",
          "format": "epoch_second"
        },
        "Last Modified Date Time": {
          "type": "date",
          "format": "epoch_second"
        },
        "Re opened Date": {
          "type": "date",
          "format": "epoch_second"
        },
        "Closed Date": {
          "type": "date",
          "format": "epoch_second"
        },
        "New Time": {
          "type": "date",
          "format": "epoch_second"
        },
        "Assigned Time": {
          "type": "date",
          "format": "epoch_second"
        },
        "In Progress Time": {
          "type": "date",
          "format": "epoch_second"
        },
        "Cancelled Time": {
          "type": "date",
          "format": "epoch_second"
        },
        "Pending Time": {
          "type": "date",
          "format": "epoch_second"
        },
        "Impacted Customers": {
          "type": "number"
        }
      }
    }
  }
}
```
# Installation / Setup

## Server

* AWS IP: 54.217.237.133
* username: virgin


## Installed Components

The following components were instralled on the server. 

* ElasticSearch at: `/home/virgin/opt/elasticsearch-5.2.0`
* Logstash at: `/home/virgin/opt/logstash-5.2.0`
* Kibana at: `/home/virgin/opt/kibana-5.2.0-linux-x86_64`
* JDK 8 at: `/home/virgin/opt/jdk1.8.0_121`

The following link brings you to a page that provides a description of the simple steps involved in installing the individual ELK components: [Elasticsearch Getting Started](https://www.elastic.co/start)

## .bash updates

Added the following to .bash_profile

```
JAVA_HOME=/home/virgin/opt/jdk1.8.0_121
export JAVA_HOME
PATH=$JAVA_HOME/bin:$PATH
export PATH
```

# Useful Commands

## Run logstash 

`/home/virgin/opt/logstash-5.2.0/bin/logstash -f ~/data/demo/conf/virgin-logstash.conf`

## Test Your Logstash Config File

From your logstash home directory:

`/home/virgin/opt/logstash-5.2.0/bin/logstash --log.level debug -t -f ~/data/demo/conf/virgin-logstash.conf`
 
## Get health of cluster:

`curl -XGET 'http://localhost:9200/_cluster/health?pretty=true'`

## Get list of indexes

`curl 'localhost:9200/_cat/indices?v'`

## Get index stats

`curl localhost:9200/_stats`

## Delete index
  
`curl -X DELETE 'http://localhost:9200/virgin-tt-2017.02.01'`

## Create template
`curl -XPUT http://localhost:9200/_template/virgin_tt_template -d '<< insert definition here>>';`

## Print out template

`curl -XGET localhost:9200/_template/virgin_tt_template?pretty`

## Delete the Template

`curl -XDELETE localhost:9200/_template/virgin_tt_template`

# Sample Dashboards

As part of the demonstration to Virgin Media, the following dashboards were created.

## Service Manager Dashboard

This dashboard is aimed at staff who are referred to as Service Managers. The idea is that they could use it to understand the performance of
individual services over time.

![alt text](/test/Dashboards/ServiceManagerDashboard.png "Service Manager Dashboard") 

**Possibly issues with this dashboard:**

* It is just based on one country, whereas the real one would provide a drill down to individual countries.
* It should be extended to also provide a breakdown of Number of Impacted customers.
* The Service names currently used in the Tickets provided are not useful. There is a 'data modelling/ cleansing' exercise taking place 
within Virgin Media to tidy up the values used in Tickets to improve its usefulness.  

## Service Operations Dashboard

This dashboard was aimed at staff who are referred to as Service Operations. The idea is that it could be used by them to 
provide operational support on specific services and how they are performing right now.

![alt text](/test/Dashboards/ServiceOperationsDashboard.png "Service Operations Dashboard") 

**Possibly issues with this dashboard:**

* Following our meeting, the impression given by Robert Nolan in Virgin Media was that the operations staff already had enough information with their current tools and that they probably don't need another dashboard to do their job.


## Exec Dashboard

This dashboard is targetted at senior execs who are on the move (e.g. using iphone) and want a snapshot view as to whether there is anything wrong or not right now and if so, how severe is the problem.
This dashboard was demonstrated as an iframe within a simple html page to show how it could be displayed on a smaller screen device without Kibana logo on the left.
The file `src/execdashboard.html` was used in this case.
 
![alt text](/test/Dashboards/ExecDashboard.png "Exec Dashboard") 
 
**Possibly issues with this dashboard:**

* It is just based on one country, whereas the real one would provide a drill down to individual countries.
* It is intended as near realtime which could be an issue if we cannot load the data in near realtime.
* It should be extended to also provide a breakdown of Number of Impacted customers.
* The Service names currently used in the Tickets provided are not useful. There is a 'data modelling/ cleansing' exercise taking place 
within Virgin Media to tidy up the values used in Tickets to improve its usefulness.  


# Helpful Info

* ES/ Kibana Search Query Syntax https://lucene.apache.org/core/2_9_4/queryparsersyntax.html
* Convert Epoch Seconds to Date/ Time `http://www.epochconverter.com/`
